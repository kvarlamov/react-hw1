﻿import React, {Component} from "react";
import classes from './Auth.module.css'
import Input from "../UI/Input/Input";
import Button from "../UI/Button/Button";

function createFormControls() {
    return {
        email: {
                label: 'Email',
                type: 'email'
            },
        password: {
                label: 'Password',
                type: 'password'
            }
        }
}

class Auth extends Component {

    state = {
        formControls: createFormControls()
    }

    submitHandler = event => {
        event.preventDefault();
    }

    renderInputs() {
        return Object.keys(this.state.formControls).map((controlName, index) => {
            const control = this.state.formControls[controlName];

            return (
                <Input
                    key={controlName + index}
                    label={control.label}
                    type={control.type}
                    value={control.value}
                    touched={control.touched}
                    onChange={event => this.changeHandler(event.target.value, controlName)}
                    onBlur={this.onBlurHandler.bind(this, controlName)}
                />
            )
        })
    }

    changeHandler(value, controlName) {
        const formControls = {...this.state.formControls};
        const control = {...formControls[controlName]};
        
        control.value = value;
        
        formControls[controlName] = control;
        
        this.setState({
            formControls
        })
    }

    onBlurHandler(controlName) {
        const formControls = {...this.state.formControls};
        const control = {...formControls[controlName]};
        
        control.touched = true;
        
        formControls[controlName] = control;
        
        this.setState({formControls});
    }

    loginHandler = () => {
        const authData = {
            email: this.state.formControls.email.value,
            password: this.state.formControls.password.value
        }
        
        console.log(authData);
    }

    render() {
        return (
            <div className={classes.Auth}>
                <div>
                    <h1>Login</h1>
                    <form onSubmit={this.submitHandler}>
                        {this.renderInputs()}
                    </form>
                    <Button
                        type="primary"
                        onClick={this.loginHandler}
                    >
                        Login
                    </Button>                    
                    <span style={{color: 'red'}}>{this.state.mainError}</span>                    
                </div>                
            </div>
        )
    }
}

export default Auth;